           //observeHeadearVisibility();
           function observeHeadearVisibility(){
            / *Header-Scroll control */
            let lastChecked = 0;
            let isScrollFixed = false;

            setInterval(()=>{
                let actualScroll = window.scrollY;
                const headerElem = document.querySelector('header');
                if(actualScroll < lastChecked && !isScrollFixed){
                    headerElem.classList.add('fixed-header');
                    isScrollFixed = !isScrollFixed;
                }else if(actualScroll > lastChecked && isScrollFixed){
                    headerElem.classList.remove('fixed-header');
                    isScrollFixed = !isScrollFixed;
                }else if(actualScroll == 0){
                    headerElem.classList.remove('fixed-header');
                }
                lastChecked = actualScroll;
            },250);
        }
        document.onreadystatechange = ()=>{
            AOS.init({
                 duration: 1200,
            })
        }
